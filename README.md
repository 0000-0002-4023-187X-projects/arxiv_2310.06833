This source code has been used in the following paper: _Milestoning estimators of dissipation in systems observed at a coarse resolution: When ignorance is truly bliss_ 

Authors: Kristian Blom, Kevin Song, Etienne Vouga, Aljaz Godec, Dmitrii Makarov

The following version of Python was used: 3.9.12

The following Python packages are required to run the source code: matplotlib, numpy, math, and numba.

Summary of the main files:

- Ring-Notebook.ipynb: Jupyter notebook to calculate the entropy production for the discrete-time Markov chain shown in Figure 1a (model A) and Figure S2a (model B). The notebook also contains the raw data for the results shown in Figures 2, S2, and S3. 

- Single-File-Notebook.ipynb: Jupyter notebook to calculate the entropy production + waiting-time distributions for the discrete-time single-file system shown in Figure 3. The notebook also contains the raw data for the results shown in Figures 3, 5, and S5.

- ring_simulations_tools.py: Source code for stochastic trajectories of the discrete-time Markov chain shown in Figure 1a (model A) and Figure S2a (model B). For an example of stochastic trajectories see Figure 1b-c and S4a-c.

- single_file_simulations_tools.py: Source code for stochastic trajectories of the discrete-time single-file system shown in Figure 2. For an example of stochastic trajectories see Figure S4d-f. 

- entropy_tools.py: Source code to determine the entropy production from a given stochastic trajectory. 

- waiting_time_tools.py: Source code to determine the waiting-time distribution for 3 milestones from a given stochastic trajectory. This code also contains the analytical expressions for the single-file waiting-time distribution.  

- Raw_data_waiting_times: Contains the raw data (.npy files) of the waiting-time distributions shown in Figures 5 and S3.

Please make sure all above files are in the same folder before executing the Jupyter notebooks. 
