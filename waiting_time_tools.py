#Analytical packages
import numpy as np

#Numba package
import numba as nb
from numba import njit, b1, i1, i8, f8

#--------------------------------------
#--------------------------------------

def waiting_time_module(traj):
    
    #Step 1: Calculate all waiting times
    waiting_times = np.diff(np.where(np.diff(traj)!=0)[0], prepend = -1)
    
    #Step 2: Remove duplicates in the trajectory
    traj_short = traj[np.insert(np.diff(traj).astype(np.bool), 0, True)]
    
    #Step 3: Collect statistics for each of the milestones
    psi0min, psi0plus, psi1plus, psi1min, psi2min, psi2plus = waiting_time_analyzer(waiting_times, traj_short)
    
    return(psi0min, psi0plus, psi1plus, psi1min, psi2min, psi2plus)

#--------------------------------------

@njit('Tuple((i8[:],i8[:],i8[:],i8[:],i8[:],i8[:]))(i8[:],i8[:])')
def waiting_time_analyzer(waiting_times, traj_short):
    
    #Set temporary
    temp = len(waiting_times)
    
    #Initialize counters
    count1, count2, count3, count4, count5, count6 = 0,0,0,0,0,0
    
    #Initialize data arrays for each milestone (assuming 3 milestones)
    psiaplus, psiamin = np.zeros((temp), dtype = i8), np.zeros((temp), dtype = i8)
    psibplus, psibmin = np.zeros((temp), dtype = i8), np.zeros((temp), dtype = i8)
    psicplus, psicmin = np.zeros((temp), dtype = i8), np.zeros((temp), dtype = i8)
    
    #Collect waiting-time statistics (assuming 3 milestones)
    for i in range(1,temp):
        if traj_short[i] == 0:
            if traj_short[i-1] == 1 and traj_short[i+1] == 2: 
                psiamin[count1] = waiting_times[i]
                count1 += 1
            if traj_short[i-1] == 2 and traj_short[i+1] == 1: 
                psiaplus[count2] = waiting_times[i]
                count2 += 1
        elif traj_short[i] == 1:
            if traj_short[i-1] == 0 and traj_short[i+1] == 2: 
                psibplus[count3] = waiting_times[i]
                count3 += 1
            if traj_short[i-1] == 2 and traj_short[i+1] == 0: 
                psibmin[count4] = waiting_times[i]
                count4 += 1
        else:
            if traj_short[i-1] == 0 and traj_short[i+1] == 1: 
                psicmin[count5]  = waiting_times[i]
                count5 += 1
            if traj_short[i-1] == 1 and traj_short[i+1] == 0: 
                psicplus[count6] = waiting_times[i]
                count6 += 1
                
    return(psiamin[0:count1], psiaplus[0:count2], 
           psibplus[0:count3], psibmin[0:count4], 
           psicmin[0:count5], psicplus[0:count6])

#--------------------------------------
#--------------------------------------

def analytical_waiting_times(Nsites,Nmilestones,pbias,k_arr):

    #Initialize data arrays
    psi0plusana, psi0minana = np.zeros(len(k_arr)), np.zeros(len(k_arr))
    psi1plusana, psi1minana = np.zeros(len(k_arr)), np.zeros(len(k_arr))
    psi2plusana, psi2minana = np.zeros(len(k_arr)), np.zeros(len(k_arr))
    
    #Construct milestonelist
    milestonelist = np.ceil(Nsites*np.arange(0,Nmilestones,1)/Nmilestones)
    
    #Construct milestone distances
    dist_arr = np.mod(np.roll(milestonelist, -1)-milestonelist,Nsites)-1

    #Set milestone territories
    Delta0min, Delta0plus = dist_arr[-1]*(Nsites-1)+1, (dist_arr[0]+1)*(Nsites-1)
    Delta1min, Delta1plus = dist_arr[0]*(Nsites-1)+1, (dist_arr[1]+1)*(Nsites-1)
    Delta2min, Delta2plus = dist_arr[1]*(Nsites-1)+1, (dist_arr[2]+1)*(Nsites-1)
    Delta0 = np.int(Delta0min + Delta0plus)
    Delta1 = np.int(Delta1min + Delta1plus)
    Delta2 = np.int(Delta2min + Delta2plus)

    #Construct analytical solutions (see Eq. (20) in manuscript)
    for i in range(1, Delta0): 
        psi0plusana += np.cos(i*np.pi/Delta0)**(k_arr-1)*np.sin(i*np.pi/Delta0)*np.sin(i*np.pi*Delta0plus/Delta0)
        psi0minana  += np.cos(i*np.pi/Delta0)**(k_arr-1)*np.sin(i*np.pi/Delta0)*np.sin(i*np.pi*Delta2plus/Delta0)
    for i in range(1, Delta1): 
        psi1plusana += np.cos(i*np.pi/Delta1)**(k_arr-1)*np.sin(i*np.pi/Delta1)*np.sin(i*np.pi*Delta1plus/Delta1)
        psi1minana  += np.cos(i*np.pi/Delta1)**(k_arr-1)*np.sin(i*np.pi/Delta1)*np.sin(i*np.pi*Delta0plus/Delta1)
    for i in range(1, Delta2): 
        psi2plusana += np.cos(i*np.pi/Delta2)**(k_arr-1)*np.sin(i*np.pi/Delta2)*np.sin(i*np.pi*Delta2plus/Delta2)
        psi2minana  += np.cos(i*np.pi/Delta2)**(k_arr-1)*np.sin(i*np.pi/Delta2)*np.sin(i*np.pi*Delta1plus/Delta2)
    
    #Prefactors
    temp  = (4*(1-pbias)*pbias)**(k_arr/2)
    temp2 = 1/pbias-1
    psi0plusana *= temp*temp2**(Delta0plus/2)
    psi0minana  *= temp*temp2**(-Delta2plus/2)
    psi1plusana *= temp*temp2**(Delta1plus/2)
    psi1minana  *= temp*temp2**(-Delta0plus/2)
    psi2plusana *= temp*temp2**(Delta2plus/2)
    psi2minana  *= temp*temp2**(-Delta1plus/2)
    
    #Normalize
    psi0plusana /= np.sum(psi0plusana)
    psi0minana  /= np.sum(psi0minana) 
    psi1plusana /= np.sum(psi1plusana)
    psi1minana  /= np.sum(psi1minana) 
    psi2plusana /= np.sum(psi2plusana)
    psi2minana  /= np.sum(psi2minana) 
    
    return(psi0minana,  psi0plusana, 
           psi1plusana, psi1minana, 
           psi2minana,  psi2plusana)